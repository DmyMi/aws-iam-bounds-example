# General flow

## Super Administrator actions

1. **Administrator** step 1: Define "end user" permissions boundary (example in `iam-boundary.json`) and name it `IAMUserPermissionBoundary`
2. **Administrator** step 2: Define the permissions boundary for delegated admins (example in `admin-boundary.json`) so that they can create only bound users, and name it `IAMAdminPermissionBoundary`.
3. **Administrator** step 3: Create delegated admin and attach the permissions policy (e.g., `AdministratorAccess`) providing the admin permissions boundary (`IAMAdminPermissionBoundary`). 

Delegated admin now has the ability to create users but must always attach a boundary policy.

## Delegated administrator actions

1. **Delegated admin** step 1: Create a user and attach the permissions policy providing the user permissions boundary (`IAMUserPermissionBoundary`).

Now user can have any kinds of permissions (even full admin access), but those permissions will be restrited by boundary.

> NOTE: Replace `<ACCOUNT_NUMBER>` with your actual account number :)
